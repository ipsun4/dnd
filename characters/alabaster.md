# Bardon alabaster

## Description
Bardon was born of an elven mother and human father that never should have met in the first place. Most refer to him as "Alabastard," playing on his mother's maiden name, which he took.  Though most ridicule him, Bardon keeps a cool head and a quick tongue, for it gets him out of trouble. A wealthy businessman took note of his talents while passing him by, and offered him a job as a bookkeeper/scribe. Bardon took it, and though he became talented at his craft, he quickly regretted his decision. That is, until one special customer offered him a chance at a new life...

## Stats
| Stat | Points |
|---|---|
|STR|13|
|DEX|8|
|CON|12|
|INT|11|
|WIS|7|
|CHA|15|

## Abilities
* Darkvision (60ft)
* 4/6 chance find secret door
* +2 save against poison
