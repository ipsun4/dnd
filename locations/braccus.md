# Port Braccus
a coastal metropolis home of the Great Braccus Statue, the living demigod Braccus the Mighty and crab delicacies

# Political
* Braccus is run by the god-titan Braccus. Braccus is a 10 foot tall Crab that speaks english and has gained notoriety for defending the port from pirates, and sea monsters.
