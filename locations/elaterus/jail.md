# Elaterus Jail
A small diminuative jail and armory for the township of Elaterus

### Armory
The Armory contains various spears and pikes to defend against invaders or barbarians

### Jail
Inside the Jail cell is a diminuative gnome, [Ferim the Unfortunate](characters/ferim.md). He cries out that it wasn't him who stole grain from Morel's farm! The grain was missing before he passed through.
