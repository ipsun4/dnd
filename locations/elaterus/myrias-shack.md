# Myrias's Shack
home to the [Hermit Myrias]

## Description
A dark dank shack made of rotting swamp wood. Inside are a work table with large shears and hacksaws, a small cot, and rotten food.

### Things that can be found
* Notebooks
  * anatomy writings of click beetles and larvae
  * large shears
    * rust near base of shears has been worn off
  * 
