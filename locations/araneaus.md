# Araneaus

### Medieval Milan known for garments spun with spider silk

## Geography
Araneaus is a medium-sized city-state built in the crevasse between mountains. Up in the tallest mountain to the north, Mt. Silk, the economy is driven by harvesting silk from docile domesticated giant spiders. From this mountain also flows a waterfall spewing from the internal cave system that the spiders live in. This waterfall winds down the mountain into a canal that divides the city into the east and west districts.

## City attributes and economy
* Lower Class -> Giant Spider milkers
  * Serfs of the caves where spider silk is milked from docile domesticated Giant Spiders (Size of Large Dog/Razorback Pig)
  * Industrial loomsmen
    * Industrial workers of factories of small to medium sized silk looms
* Middle Class
  * Seamstress/garment workers
    * industrial sweatshops, but paid well with good work/life balance
* Upper class
  * Fashion Designers
  * Textile Industry plant owners

## Ideas for city conflict
* Spider silk woven mummys
  * cavern mini-boss
* Dead fashion model
  * Models are live marionettes controlled with spider silk threads
  * Killed because the Father was building an aqueduct to the city that would stop the Spider queen from controlling and tampering with the water source for mind control
