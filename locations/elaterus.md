# Elaterus Township
A small township of subsistence farmers

## Geography
The Elaterus Township is a small egregarian township on the marsh delta between the mountain City of [Araneaus](araneaus.md) and the coastal town of [Port Braccus](braccus.md).

## Locations
* [Strider Tavern](elaterus/strider-tavern.md)
* [Lake Strider](elaterus/lake-strider.md)
* [Lawman's office and Jail](elaterus/jail.md)
