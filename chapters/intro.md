# Intro: Elation in Elaterus

## Wrangling the Click Beetle

Amongst your travels, you find yourself getting ready to go to finish your day of traveling at the Strider Tavern, a small in the township of Elaterus: a small egrarian farming town in the delta between the mountain City of [Araneaus](araneaus.md) and the coastal trade post of [Port Braccus](braccus.md). The tavern is small, but cozy, a fire crackles on the hearth, and the roast stew smells hearty and filling. After a loud thump and screams, a female farmer runs in and screams for help.

### Combat
| Enemy | HP | Attack | Damage |
|---|---|---|---|
| Click Beetle | 20 | +4 | 1d8 |
| Lawman | 13 | +11 | 1d4 +1 punch, 1d8 broadsword |

## Aftermath
Woman (Jenn) runs up in anger/happiness for aftermath of her click beetle. Her pendant has the same insignia of the brand on the click beetle. She complains of the hermit to the west messing with her crops and beetles. She asks you to further investigate

## Jenn's Field
Investigate crops
### things that can be found
* flattened rice is in random circles
* broken off antennae on the ground
* humanoid footprints from west toward hermit Myras Shack
* scrap of paper
  * check for scientific writing

## Myrias' shack
look on page

## Back at town square
* Myrias is on the back of a giant centipede

| Enemy | HP | Attack | Damage |
|---|---|---|---|
| Hermit Myrias | 10 (after 2 damage, helmet falls off) | +4 | 1d4 punch |
| Centipede | 30 | +11 | 1d4 +1 punch, 1d8 broadsword |
