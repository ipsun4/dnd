# Locations
![](locations/world-map.png "The world")

## Cities
* [Araneaus](locations/araneaus.md)
* [Melisse](locations/melisse.md)
* [Port Braccus](locations/braccus.md)

## Towns
* [Elaterus Township](locations/elaterus.md)
